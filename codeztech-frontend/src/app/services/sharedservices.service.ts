import { BehaviorSubject, Observable } from 'rxjs';

import { AuthenticationService } from '../auth/authentication.service';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Userdetails } from '../user.model';
import { environment } from '../../environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class SharedservicesService {
  private currentUserSubject: BehaviorSubject<Userdetails>;
  public currentUser: Observable<Userdetails>;
  public currentUserLoggedIn = this.authenticationService.currentUserValue;

  public isUserLogin = this.currentUserLoggedIn !== null ? true : false;
  public headers = new HttpHeaders()
    .set('content-type', 'application/json')
    .set('Access-Control-Allow-Origin', '*');

  // mongodb local uri
  // public uri = environment.productionurl;
  public uri = environment.localurl;

  constructor(private http: HttpClient, private authenticationService: AuthenticationService) {
    this.currentUserSubject = new BehaviorSubject<Userdetails>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): Userdetails {
    return this.currentUserSubject.value;
  }

  public getAllProfileInformation() {
    return this.http.get(`${this.uri}/users`);
  }

  public getLoggedUserInformation(data) {
    return this.http.post(`${this.uri}/logged/users`, data);
  }

  public userProfileInformationUpdate(data) {
    return this.http.post(`${this.uri}/users/profile/update`, data);
  }

  public uploadUserProfileImage(data) {
      return this.http.post(`${this.uri}/users/profile/picture/upload`, data);
  }

  public submitArticleInformation(data) {
    return this.http.post(`${this.uri}/users/article/post`, data);
  }

  public userArticleComments(data) {
    return this.http.post(`${this.uri}/users/article/comments`, data);
  }

  public getArticleInformation(data) {
    return this.http.post(`${this.uri}/users/article/get`, data);
  }

  public fetchAllComments(data) {
    return this.http.post(`${this.uri}/users/article/fetchall/comments`, data);
  }

  public deleteMainComments(data) {
    return this.http.post(`${this.uri}/users/delete/articles`, data);
  }

  public updateMainArticles(data) {
    return this.http.post(`${this.uri}/users/udpate/articles`, data);
  }

  public deleteSubComments(data) {
    return this.http.post(`${this.uri}/users/delete/comments`, data);
  }

  public userTrackReactions(data) {
    return this.http.post(`${this.uri}/users/track/reactions`, data);
  }

}
