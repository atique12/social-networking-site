export interface Userdetails {
    name: String;
    phone: String;
    profileurl: String;
    headline: String;
    userbio: String;
    dob: String;
    email: String;
    password: String;
}

