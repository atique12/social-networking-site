import { NgModule } from '@angular/core';

import {
    MatButtonModule,
    MatToolbarModule,
    MatListModule,
    MatIconModule,
    MatCardModule,
    MatTabsModule,
    MatFormFieldModule,
    MatDividerModule,
    MatStepperModule,
    MatMenuModule,
    MatSidenavModule,
    MatInputModule,
    MatGridListModule,
    MatTableModule,
    MatSelectModule,
    MatDialogModule,
    MatButtonToggleModule,
    MatSlideToggleModule,
    MatCheckboxModule,
    MatChipsModule,
    MatPaginatorModule
  } from '@angular/material';

  @NgModule({
    imports: [
      MatButtonModule,
      MatToolbarModule,
      MatListModule,
      MatIconModule,
      MatCardModule,
      MatTabsModule,
      MatFormFieldModule,
      MatDividerModule,
      MatStepperModule,
      MatMenuModule,
      MatSidenavModule,
      MatInputModule,
      MatGridListModule,
      MatTableModule,
      MatSelectModule,
      MatDialogModule,
      MatButtonToggleModule,
      MatSlideToggleModule,
      MatCheckboxModule,
      MatChipsModule,
      MatPaginatorModule
      // TabsModule.forRoot()
    ],
    exports: [
      MatButtonModule,
      MatToolbarModule,
      MatListModule,
      MatIconModule,
      MatCardModule,
      MatTabsModule,
      MatFormFieldModule,
      MatDividerModule,
      MatStepperModule,
      MatMenuModule,
      MatSidenavModule,
      MatInputModule,
      MatGridListModule,
      MatTableModule,
      MatSelectModule,
      MatDialogModule,
      MatButtonToggleModule,
      MatSlideToggleModule,
      MatCheckboxModule,
      MatChipsModule,
      MatPaginatorModule
      // TabsModule
    ]
  })
  export class MaterialModule {}
  