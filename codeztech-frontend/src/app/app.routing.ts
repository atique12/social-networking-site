import { RouterModule, Routes } from '@angular/router';

import { AuthGuard } from './guards/app.authguard';
import { DashboardComponent } from './modules/dashboard/dashboard.component';
import { LoginComponent } from './modules/login/login.component';
import { ModuleWithProviders } from '@angular/core';
import { ProfileSettingsComponent } from './modules/dashboard/profile-settings/profile-settings.component';
import { RegisterComponent } from './modules/register/register.component';

// import { DetailsComponent } from './modules/dashboard/dashboard/details/details.component';
export const ROUTES: Routes = [
    {path: '', component: LoginComponent, pathMatch: 'full'},
    {path: 'app/dashboard', component: DashboardComponent, canActivate: [AuthGuard]},
    {path: 'app/dashboard/profile', component: ProfileSettingsComponent, canActivate: [AuthGuard]},
    {path: 'registration', component: RegisterComponent},
];

export const ROUTING: ModuleWithProviders = RouterModule.forRoot(ROUTES);
