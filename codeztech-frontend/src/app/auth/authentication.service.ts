import { BehaviorSubject, Observable } from 'rxjs';

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { OnInit } from '@angular/core';
import { Userdetails } from '../user.model';
import { environment } from '../../environments/environment';
import { map } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class AuthenticationService implements OnInit  {
    private currentUserSubject: BehaviorSubject<Userdetails>;
    public currentUser: Observable<Userdetails>;

    // public uri = environment.productionurl;
    public uri = environment.localurl;
    constructor(private http: HttpClient) {
      this.currentUserSubject = new BehaviorSubject<Userdetails>(JSON.parse(localStorage.getItem('currentUser') || '{}' ));
      this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue(): Userdetails {
        return this.currentUserSubject.value;
    }

    // tslint:disable-next-line: contextual-lifecycle
    ngOnInit() {
    }

    login(user) {
      console.log('inside authentication service');
      return this.http.post<any>(`${this.uri}/users/login`, user)
      .pipe(map(auser => {
                // login successful if there's a jwt token in the response
                if (auser) {
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('currentUser', JSON.stringify(auser));
                    this.currentUserSubject.next(auser);
                }
                return auser;
            }));
    }

    public sharedUpdatedUserDetails(data) {
      this.currentUserSubject.next(data);
    }

    logout() {
        // remove user from local storage to log user out
        console.log('Logout !!');
        localStorage.removeItem('currentUser');
        this.currentUserSubject.next(null);
  }

}
