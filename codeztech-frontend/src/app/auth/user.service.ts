import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Userdetails } from '../user.model';
import { environment } from '../../environments/environment';

@Injectable({ providedIn: 'root' })
export class UserService {
    // public uri = environment.productionurl;
    public uri = environment.localurl;
    constructor(private http: HttpClient) { }

    getAll() {
        return this.http.get<Userdetails[]>(`${this.uri}/users`);
    }

    register(user: Userdetails) {
        return this.http.post(`${this.uri}/users/register`, user);
    }
}
