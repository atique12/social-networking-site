import { Component, OnInit } from '@angular/core';

import { AuthenticationService } from '../../auth/authentication.service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Userdetails } from '../../user.model';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  currentUser: Userdetails;
  currentUserSubscription: Subscription;
  users: Userdetails[] = [];
  constructor(private router: Router, private authenticationService: AuthenticationService, private http: HttpClient) {
    this.currentUserSubscription = this.authenticationService.currentUser.subscribe(user => {
      this.currentUser = user;
    });
  }
  navigate() {
    this.router.navigate(['/app/dashboard']);
  }
  ngOnInit() {
  }
  logout() {
    this.router.navigate(['/']);
    this.authenticationService.logout();
  }
}
