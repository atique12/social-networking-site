import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../../auth/user.service';
import { HttpClient } from '@angular/common/http';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';

// import TinyDatePicker from 'tiny-date-picker';
declare var $: any;
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  password: any;
  userName: any;
  userDateofBirth: any;
  isMature: any;
  npassword: any;
  ncpassword: any;
  select: any;
  email: any;
  mail: any;
  phone: any;
  isChecked: boolean;
  constructor(private router: Router, private ref: UserService, private http: HttpClient) {
    this.isChecked = true;
  }
  ngOnInit() {
  }
  checkphone(e) {
    this.phone = e;
    var strlength = this.phone.toString();
    if (strlength.length === 10) {
      var mailerr = <HTMLElement>document.getElementById('phonevalid');
      mailerr.style.display = 'block';
      var mailer = <HTMLElement>document.getElementById('phoneerr');
      mailer.style.display = 'none';
      const btn = <HTMLInputElement>document.getElementById('button');
      btn.disabled = false;
    }
    else {
      var mailerr = <HTMLElement>document.getElementById('phonevalid');
      mailerr.style.display = 'none';
      var mailer = <HTMLElement>document.getElementById('phoneerr');
      mailer.style.display = 'block';
      // var mail = <HTMLElement>document.getElementById('phoneHelp');
      // mail.style.display = 'none';
    }
  }
  checkm(e) {
    this.email = e;
    var pattern = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*\.\w{2,3}/;
    if (e.match(pattern)) {
      var mailerr = <HTMLElement>document.getElementById('mailvalid');
      mailerr.style.display = 'block';
      var mailer = <HTMLElement>document.getElementById('mailerr');
      mailer.style.display = 'none';
      // var mail = <HTMLElement>document.getElementById('emailHelp');
      // mail.style.display = 'none';
      var btn = <HTMLInputElement>document.getElementById('button');
      btn.disabled = false;
    }
    else {
      var mailerr = <HTMLElement>document.getElementById('mailvalid');
      mailerr.style.display = 'none';
      var mailer = <HTMLElement>document.getElementById('mailerr');
      mailer.style.display = 'block';
      // var mail = <HTMLElement>document.getElementById('emailHelp');
      // mail.style.display = 'none';
    }
  }

  checkpw(e) {
    this.npassword = e;
    var pattern = /^.{6,}$/;
    if (e.match(pattern)) {
      var mailerr = <HTMLElement>document.getElementById('pwvalid');
      mailerr.style.display = 'block';
      var mailer = <HTMLElement>document.getElementById('pwerr');
      mailer.style.display = 'none';
      var btn = <HTMLInputElement>document.getElementById('button');
      btn.disabled = false;
    }
    else {
      var mailerr = <HTMLElement>document.getElementById('pwvalid');
      mailerr.style.display = 'none';
      var mailer = <HTMLElement>document.getElementById('pwerr');
      mailer.style.display = 'block';
    }
  }
  checkAgentName(e) {
    this.userName = e;
    if (this.userName !== '') {
      var validName = <HTMLElement>document.getElementById('agentNameValid');
      validName.style.display = 'block';
      var invalidName = <HTMLElement>document.getElementById('agentNameErr');
      invalidName.style.display = 'none';
    } else {
      var validName = <HTMLElement>document.getElementById('agentNameValid');
      validName.style.display = 'none';
      var invalidName = <HTMLElement>document.getElementById('agentNameErr');
      invalidName.style.display = 'block';
    }
  }
  checkcpw(e) {
    this.ncpassword = e;
    if (this.npassword == this.ncpassword) {
      var mailerr = <HTMLElement>document.getElementById('cpwvalid');
      mailerr.style.display = 'block';
      var mailer = <HTMLElement>document.getElementById('cpwerr');
      mailer.style.display = 'none';
    }
    else {
      var mailerr = <HTMLElement>document.getElementById('cpwvalid');
      mailerr.style.display = 'none';
      var mailer = <HTMLElement>document.getElementById('cpwerr');
      mailer.style.display = 'block';
    }
  }
  onChange(e) {
    this.select = e;
  }
  dateCheckEvent(agentDob) {
    var dateformat = /^\d{4}-\d{2}-\d{2}$/;
    var today = new Date();
    var birthDate = new Date(agentDob);
    var age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
      age--;
    }
    this.isMature = age;
    if (agentDob.match(dateformat)) {
      var myDate = new Date(agentDob);
      var today = new Date();
      var agentBirthDate = myDate.getDate();
      var agentBirthMonth = myDate.getMonth();
      var todayDate = today.getDate();
      var todayMonth = today.getMonth();
      if ( myDate > today ) {
        var validName = <HTMLElement>document.getElementById('dateValid');
        validName.style.display = 'none';
        var invalidName = <HTMLElement>document.getElementById('dateInvalid');
        invalidName.style.display = 'block';
        var invalidName = <HTMLElement>document.getElementById('dateValidHappyBirthday');
        invalidName.style.display = 'none';
        var invalidName = <HTMLElement>document.getElementById('date18plus');
        invalidName.style.display = 'none';
      } else if(todayDate == agentBirthDate && todayMonth==agentBirthMonth && age>=18){ //Happy birthday check
        var invalidName = <HTMLElement>document.getElementById('dateValidHappyBirthday');
        invalidName.style.display = 'block';
        var validName = <HTMLElement>document.getElementById('dateValid');
        validName.style.display = 'none';
        var invalidName = <HTMLElement>document.getElementById('dateInvalid');
        invalidName.style.display = 'none';
        var invalidName = <HTMLElement>document.getElementById('date18plus');
        invalidName.style.display = 'none';
        this.userDateofBirth = agentDob;
      } else if (myDate < today) { //validate age 18+ years
        if (age >= 18) {
          var invalidName = <HTMLElement>document.getElementById('dateValidHappyBirthday');
          invalidName.style.display = 'none';
          var validName = <HTMLElement>document.getElementById('dateValid');
          validName.style.display = 'block';
          var invalidName = <HTMLElement>document.getElementById('dateInvalid');
          invalidName.style.display = 'none';
          var invalidName = <HTMLElement>document.getElementById('date18plus');
          invalidName.style.display = 'none';
          this.userDateofBirth = agentDob;
        } else if (age < 18) {
          var invalidName = <HTMLElement>document.getElementById('dateValidHappyBirthday');
          invalidName.style.display = 'none';
          var validName = <HTMLElement>document.getElementById('dateValid');
          validName.style.display = 'none';
          var invalidName = <HTMLElement>document.getElementById('dateInvalid');
          invalidName.style.display = 'none';
          var invalidName = <HTMLElement>document.getElementById('date18plus');
          invalidName.style.display = 'block';
        }
      }
    }
  }

  loginfun() {
    this.router.navigate(['/'], { queryParams: {} });
  }

  register() {
    const agentCustomerCheck  = this.isChecked;
    const passwordpattern = /^.{6,}$/;
    const emailpattern = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/;
    let email;
    let pwd;
    let confpwd;
    let phone;
    let strlength;
    const agentDOB = this.userDateofBirth;
     if (agentCustomerCheck === true) {
      email = (<HTMLInputElement>document.getElementById('exampleInputEmail1'));
      pwd = (<HTMLInputElement>document.getElementById('exampleInputPassword1'));
      confpwd = (<HTMLInputElement>document.getElementById('confpwd'));
      phone = (<HTMLInputElement>document.getElementById('number'));
      strlength = phone.value.toString();
      if (this.npassword === '' && this.email === '' && this.password === '' && this.phone === '') {
        let mailerr = <HTMLElement>document.getElementById('filled');
        mailerr.style.display = 'block';
        let btn = <HTMLInputElement>document.getElementById('button');
        btn.disabled = true;
      // tslint:disable-next-line: max-line-length
      } else if (email.value.match(emailpattern) && pwd.value === confpwd.value && pwd.value.match(passwordpattern) && strlength.length === 10 && this.isMature >= 18 && this.userName !== '') {
        const userData = {
          name: this.userName,
          profileurl: '',
          headline: '',
          userbio: '',
          phone: phone.value,
          dob: this.userDateofBirth,
          email: this.email,
          password: pwd.value,
        };
        this.ref.register(userData).subscribe((data) => {
          const res = JSON.parse(JSON.stringify(data));
          if (res.status === 200) {
            this.router.navigate(['/']);
          } else {
            const mailerr = <HTMLElement>document.getElementById('filled');
            mailerr.style.display = 'block';
          }
       });
      } else {
        const mailerr = <HTMLElement>document.getElementById('EmailPassErr');
        mailerr.style.display = 'block';
      }
    }
  }
}
