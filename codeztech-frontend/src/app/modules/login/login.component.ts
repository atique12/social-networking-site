import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../auth/authentication.service';
import { HttpClient } from '@angular/common/http';
import { first } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { Userdetails } from '../../user.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  password: any;
  email: any;
  forgotemail: any;
  npassword: any;
  getUserDetails: any;

  currentUser: Userdetails;
  currentUserSubscription: Subscription;
  users: Userdetails[] = [];
  constructor(private router: Router, private auth: AuthenticationService, private http: HttpClient) { 
    this.currentUserSubscription = this.auth.currentUser.subscribe(user => {
      this.currentUser = user;
      if (this.currentUser) {
        this.router.navigate(['/app/dashboard']);
      } else {
        this.router.navigate(['/']);
      }
    });
  }

  ngOnInit() {
  }
  checkm(e) {
    this.email = e;
    const pattern = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/;
    if (e.match(pattern)) {
      // tslint:disable-next-line: no-shadowed-variable
      const mailerr = <HTMLElement>document.getElementById('mailvalid');
      mailerr.style.display = 'block';
      const mailer = <HTMLElement>document.getElementById('mailerr');
      mailer.style.display = 'none';
      const mail = <HTMLElement>document.getElementById('emailHelp');
      mail.style.display = 'none';
      const btn = <HTMLInputElement>document.getElementById('button');
      btn.disabled = false;
    } else {
      const mailerr = <HTMLElement>document.getElementById('mailvalid');
      mailerr.style.display = 'none';
      const mailer = <HTMLElement>document.getElementById('mailerr');
      mailer.style.display = 'block';
      const mail = <HTMLElement>document.getElementById('emailHelp');
      mail.style.display = 'none';
    }
  }
  checkpw(e) {
    this.npassword = e;
    const pattern = /^.{6,}$/;
    if (e.match(pattern)) {
      // tslint:disable-next-line: no-shadowed-variable
      const mailerr = <HTMLElement>document.getElementById('pwvalid');
      mailerr.style.display = 'block';
      // tslint:disable-next-line: no-shadowed-variable
      const mailer = <HTMLElement>document.getElementById('pwerr');
      mailer.style.display = 'none';
      const btn = <HTMLInputElement>document.getElementById('button');
      btn.disabled = false;
    } else {
      const mailerr = <HTMLElement>document.getElementById('pwvalid');
      mailerr.style.display = 'none';
      const mailer = <HTMLElement>document.getElementById('pwerr');
      mailer.style.display = 'block';
    }
  }
  checkm1(e) {
    this.forgotemail = e;
    const pattern = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;
    if (e.match(pattern)){
       const mailerr = <HTMLElement>document.getElementById('mailvalid1');
       mailerr.style.display = 'block';
       const mailer = <HTMLElement>document.getElementById('mailerr1');
       mailer.style.display = 'none';
       const btn = <HTMLInputElement>document.getElementById('button1');
       btn.disabled = false;
    } else {
       const mailerr = <HTMLElement>document.getElementById('mailvalid1');
       mailerr.style.display = 'none';
       const mailer = <HTMLElement>document.getElementById('mailerr1');
       mailer.style.display = 'block';
    }
  }
  login() {
      const msg = <HTMLElement>document.getElementById('msg');
      const mpattern = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/;
      const ppattern = /^.{6,}$/;
      var email1212 = mpattern.test(this.email);
      var pass1212 =ppattern.test(this.npassword)
      console.log(email1212, pass1212)
      if (this.email.match(mpattern) && (this.npassword.match(ppattern))) {
        msg.style.display = 'none';
        // Login here
        const userData = {
          email: this.email,
          password: this.npassword
        };
        this.auth.login(userData)
        .pipe(first())
        .subscribe(
            data => {
              this.router.navigate(['/app/dashboard']);
            },
            error => {
                console.log('Error in login!');
                const mailerr = <HTMLElement>document.getElementById('EmailPassErr');
                mailerr.style.display = 'block';
                this.router.navigate(['/']);
            });
      } else {
        msg.style.display = 'block';
      }
    }
}
