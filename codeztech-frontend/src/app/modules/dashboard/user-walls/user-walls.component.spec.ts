import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserWallsComponent } from './user-walls.component';

describe('UserWallsComponent', () => {
  let component: UserWallsComponent;
  let fixture: ComponentFixture<UserWallsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserWallsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserWallsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
