import { Component, OnInit } from '@angular/core';

import { AuthenticationService } from '../../../auth/authentication.service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { SharedservicesService } from '../../../services/sharedservices.service';
import { Subscription } from 'rxjs';
import { Userdetails } from '../../../user.model';

@Component({
  selector: 'app-user-walls',
  templateUrl: './user-walls.component.html',
  styleUrls: ['./user-walls.component.scss']
})

export class UserWallsComponent implements OnInit {
  url;
  pageId: any;
  articledesc: any;
  unamedurl: any;
  articleDetails: any;
  currentUser: Userdetails;
  currentUserSubscription: Subscription;
  users: Userdetails[] = [];
  listsUser = [];

  constructor(private router: Router, private authenticationService: AuthenticationService, 
    private service: SharedservicesService, private http: HttpClient) {
    this.currentUserSubscription = this.authenticationService.currentUser.subscribe(user => {
      this.currentUser = user;
    });
    this.url = window.location.href;
    this.pageId = this.url;
  }

  ngOnInit() {
    this.fetchPostAndComments();
  }

  fetchPostAndComments() {
    const dataSet = {
       email: this.currentUser.email
    };
    this.service.getArticleInformation(dataSet).subscribe((eve) => {
      const res = JSON.parse(JSON.stringify(eve));
      this.articleDetails = res.data;
      console.log(this.articleDetails );
      this.articleDetails.filter((ss) => {
        ss.pageUrl = this.pageId + '/' + ss._id;
      });
      // Active Like and dislike Logic
      let userFound;
      for (let i = 0; i < this.articleDetails.length; i++) {
        for (let j = 0; j < this.articleDetails[i].userLikeDetails.length; j++) {
          if (this.articleDetails[i].userLikeDetails[j].userEmail === this.currentUser.email) {
            userFound = this.articleDetails[i].userLikeDetails[j].status;
            // userReaction = userFound;
            // console.log(typeof this.articleDetails[i]);
            this.articleDetails[i]['userReaction'] = userFound;
            // this.articleDetails.splice(3, 0, {
            //   currentUserReaction: userReaction
            // });
            break;
          }
        }
      }
      console.log(this.articleDetails);
      //   const likeButton = <HTMLElement>document.getElementById(this.articleDetails[i]._id);
      //   console.log(likeButton);
      //   console.log(this.articleDetails[i]._id);
      //   const decission = likeButton.classList.contains('fa-thumbs-down');
      //   if (userFound === true && decission) { //user liked
      //     likeButton.className = 'fa fa-thumbs-up font-bold dark-color';
      //     likeButton.innerHTML = ' Liked';
      //   } else if (userFound === false && !decission) { //user not liked
      //     likeButton.className = 'fa fa-thumbs-down font-bold dark-color';
      //     likeButton.innerHTML = ' Disliked';
      //   }
      //   if (counter > 0) {
      //     break;
      //   }
      // }
      // else { //user have not react anything

      // }
      this.service.fetchAllComments(dataSet).subscribe((getdata) => {
         let resData = JSON.parse(JSON.stringify(getdata));
         resData = resData.data;
         for (let i = 0; i < this.articleDetails.length; i++) {
          this.articleDetails[i].comments = [];
           for (let j = 0; j < resData.length; j++) {
              if (this.articleDetails[i]._id === resData[j].articleId) {
                this.articleDetails[i].comments.push(resData[j]);
              }
           }
         }
      });
    });
  }

  createPost() {
    const dataSet = {
      email : this.currentUser.email,
      name: this.currentUser.name,
      headline: this.currentUser.headline,
      profileurl: this.currentUser.profileurl,
      articledesc : this.articledesc
    };
    console.log(dataSet);
    if (!this.articledesc) {
      const successMessage = <HTMLElement>document.getElementById('articlenotpublished');
      const buttonAlign = <HTMLElement>document.getElementById('button');
      successMessage.style.display = 'block';
      buttonAlign.style.margin = '8px 10px 9px 34%';
      setTimeout(() => {
        successMessage.style.display = 'none';
        buttonAlign.style.margin = '8px 10px 9px 70%';
      }, 3000);
    } else {
      this.service.submitArticleInformation(dataSet).subscribe(async (event) => {
        const res = await JSON.parse(JSON.stringify(event));
        const successMessage = <HTMLElement>document.getElementById('articlepublished');
        const buttonAlign = <HTMLElement>document.getElementById('button');
        successMessage.style.display = 'block';
        buttonAlign.style.margin = '8px 10px 9px 34%';
        this.articledesc = '';
        this.fetchPostAndComments();
        setTimeout(() => {
          successMessage.style.display = 'none';
          buttonAlign.style.margin = '8px 10px 9px 70%';
        }, 2000);
      });
    }
  }
  onImgError() {
    this.unamedurl = 'https://test-live-projects.s3.ap-south-1.amazonaws.com/notnamed.png';
    this.articleDetails.filter((fs) => {
      if (!fs.profileurl) {
        fs.profileurl = this.unamedurl;
      }
    });
  }

  captureUserReaction(data) {
    const likeButton = <HTMLElement>document.getElementById(data._id);
    const decission = likeButton.classList.contains('fa-thumbs-down');
    let status = false;
    if (decission) {
      likeButton.className = 'fa fa-thumbs-up font-bold dark-color';
      likeButton.innerHTML = ' Liked';
      status = true;
    } else {
      likeButton.className = 'fa fa-thumbs-down font-bold dark-color';
      likeButton.innerHTML = ' Disliked';
      status = false;
    }
    const dataSet = {
      id: data._id,
      loggedUserEmail: this.currentUser.email,
      isTrue: status
    };
    // userTrackReactions
    this.service.userTrackReactions(dataSet).subscribe(async (event) => {
      const res = await JSON.parse(JSON.stringify(event));
      console.log('Article updated!');
      this.fetchPostAndComments();
    });
  }
  updateComments(data) {
    console.log('update comments');
    const contentId = (document.getElementById('content-text-' + data._id) as HTMLTextAreaElement).value;
    const commentPost = <HTMLElement>document.getElementById('comment-text-' + data._id);
    const commentPostErr = <HTMLElement>document.getElementById('error-text-' + data._id);
    if (contentId) {
      const dataSet = {
        id: data._id,
        article: contentId
      };
      this.service.updateMainArticles(dataSet).subscribe(async (event) => {
        const res = await JSON.parse(JSON.stringify(event));
        console.log('Article updated!');
        commentPost.style.display = 'block';
        this.fetchPostAndComments();
      });
      setTimeout(() => {
        commentPost.style.display = 'none';
      }, 3000);
    } else {
        commentPostErr.style.display = 'block';
        setTimeout(() => {
          commentPostErr.style.display = 'none';
      }, 2000);
    }
  }
  postComments(data) {
    const contentId = (document.getElementById('content-id-' + data._id) as HTMLTextAreaElement).value;
    const commentPost = <HTMLElement>document.getElementById('comment-id-' + data._id);
    const commentPostErr = <HTMLElement>document.getElementById('error-id-' + data._id);
    if (contentId) {
      // Comments insert into database
      const dataSet = {
        articleId: data._id,
        article: data.articledesc,
        articleAuthorEmail: data.email,
        commentedUserEmail: this.currentUser.email,
        commentedUserName: this.currentUser.name,
        commentedUserHeadline: this.currentUser.headline,
        commentedUserProfileUrl: this.currentUser.profileurl,
        commentedUserComments: contentId,
      };
      this.service.userArticleComments(dataSet).subscribe(async (event) => {
        const res = await JSON.parse(JSON.stringify(event));
        console.log('Article post');
        const removeContent = (document.getElementById('content-id-' + data._id) as HTMLTextAreaElement);
        removeContent.value = '';
        commentPost.style.display = 'block';
        this.fetchPostAndComments();
      });
      setTimeout(() => {
        commentPost.style.display = 'none';
      }, 3000);
    } else {
      commentPostErr.style.display = 'block';
      setTimeout(() => {
        commentPostErr.style.display = 'none';
      }, 2000);
    }
  }
  showCommentsPanel(data) {
    const commentBox = <HTMLElement>document.getElementById('comment-box-state-' + data._id);
    commentBox.style.display = 'block';
  }

  deleteSubCommentsPost(data) {
    const dataSet = {
      id: data._id
    };
    this.service.deleteSubComments(dataSet).subscribe((res) => {
       this.fetchPostAndComments();
    });
  }
  deleteMainCommentsPost(data) {
    const dataSet = {
      id: data._id
    };
    this.service.deleteMainComments(dataSet).subscribe((res) => {
       this.fetchPostAndComments();
    });
  }
  editMainCommentsPost(data) {
    const hidePost = <HTMLElement>document.getElementsByClassName('item-data-' + data._id)[0];
    hidePost.style.display = 'none';
    const showEditCommentBox = <HTMLElement>document.getElementById('edit-post-' + data._id);
    showEditCommentBox.style.display = 'block';
    const bindDataIntoCommentBox = <HTMLElement>document.getElementById('content-text-' + data._id);
    bindDataIntoCommentBox.innerHTML = data.articledesc;
  }
  showDropdownEditDeleteConfig(data, type) {
    const popup = <HTMLElement>document.getElementsByClassName('dropdown-content-' + data._id)[0];
    const popupSpanEdit = <HTMLElement>document.getElementsByClassName('design-e-' + data._id)[0];
    const popupSpanDelete = <HTMLElement>document.getElementsByClassName('design-d-' + data._id)[0];
    popup.style.display = 'block';
    popup.style.position = 'absolute';
    popup.style.minWidth = '160px';
    popup.style.boxShadow = '0px 8px 16px 0px rgba(0, 0, 0, 0.2)';
    popup.style.zIndex = '1';
    // popup.style.float = 'right';
    popup.style.marginLeft = '46%';
    popup.style.marginTop = '2%';
    popup.style.backgroundColor = '#f2f2f2';
    popup.style.padding = '1%';
    popup.style.borderTopLeftRadius = '12px';
    popup.style.borderBottomLeftRadius = '12px';
    popup.style.borderBottomRightRadius = '12px';

    if (type === 'main') {
      popupSpanEdit.style.color = 'black';
      popupSpanEdit.style.padding = '12px 16px';
      popupSpanEdit.style.textDecoration = 'none';
      popupSpanEdit.style.display = 'block';
    }


    popupSpanDelete.style.color = 'black';
    popupSpanDelete.style.padding = '12px 16px';
    popupSpanDelete.style.textDecoration = 'none';
    popupSpanDelete.style.display = 'block';

    setTimeout(() => {
      popup.style.display = 'none';
    }, 3000);
  }

  showHoverEditEnter(data) {
    const popupSpanEdit = <HTMLElement>document.getElementsByClassName('design-e-' + data._id)[0];
    popupSpanEdit.style.backgroundColor = '#ddd';
  }
  showHoverEditExit(data) {
    const popupSpanEdit = <HTMLElement>document.getElementsByClassName('design-e-' + data._id)[0];
    popupSpanEdit.style.backgroundColor = '';
  }
  showHoverEnterDelete(data) {
    const popupSpanDelete = <HTMLElement>document.getElementsByClassName('design-d-' + data._id)[0];
    popupSpanDelete.style.backgroundColor = '#ddd';
  }
  showHoverExitDelete(data) {
    const popupSpanDelete = <HTMLElement>document.getElementsByClassName('design-d-' + data._id)[0];
    popupSpanDelete.style.backgroundColor = '';
  }
}
