import { Component, OnInit } from '@angular/core';

import { AuthenticationService } from '../../../auth/authentication.service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { SharedservicesService } from '../../../services/sharedservices.service';
import { Subscription } from 'rxjs';
import { Userdetails } from '../../../user.model';
declare var $: any;
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  name: any;
  profileurl: any;
  unamedurl: any;
  headline: any;
  phone: any;
  dob: any;
  website: any;
  github: any;
  facebook: any;
  youtube: any;
  twitter: any;
  userbio: any;
  filename: any;
  imageSrc: any;
  imageLocation: any;
  uploadSuccess: boolean;
  percentDone = 0;
  currentUser: Userdetails;
  currentUserSubscription: Subscription;
  users: Userdetails[] = [];
  constructor(private router: Router, private authenticationService: AuthenticationService,
    private shared: SharedservicesService, private http: HttpClient) {
    this.currentUserSubscription = this.authenticationService.currentUser.subscribe(user => {
      this.currentUser = user;
    });
  }

  ngOnInit() {
    this.fetchDataOncard();
  }

  fetchDataOncard() {
    const dataSet = {
      email: this.currentUser.email
    };
    this.shared.getLoggedUserInformation(dataSet).subscribe((res) => {
      const result = JSON.parse(JSON.stringify(res));
      this.name = result.data.name;
      this.profileurl = result.data.profileurl;
      this.headline = result.data.headline;
      this.dob = result.data.dob;
      this.userbio = result.data.userbio;
      this.phone = result.data.phone;
      this.website = result.data.website;
      this.facebook = result.data.facebook;
      this.youtube = result.data.youtube;
      this.twitter = result.data.twitter;
      this.github = result.data.github;
      this.dob = new Date(this.dob).toISOString().split('T')[0];
   });
  }

  onImgError() {
    this.profileurl = 'https://test-live-projects.s3.ap-south-1.amazonaws.com/unnamed.png';
  }
  uploadAndProgress(files) {
    const file = files[0].size;
    if (file > 1000000) { //1000000 bytes
      return false;
    } else {
      return true;
    }
  }

  handleInputChange(e) {
    this.filename = e.target.files[0].name;
    const isStatus = this.uploadAndProgress(e.target.files);
    if (isStatus) {
      const file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];
      const pattern = /image-*/;
      const reader = new FileReader();
      if (!file.type.match(pattern)) {
        alert('invalid format');
        return;
      }
      reader.onload = this._handleReaderLoaded.bind(this);
      reader.readAsDataURL(file);
    } else {
      const imagesizelimit = (document.getElementById('imagesizelimit') as HTMLInputElement);
      // tslint:disable-next-line: variable-name
      // const uploadunsuccessful = (document.getElementById('upload-unsuccessful') as HTMLInputElement);
      // uploadunsuccessful.style.display = 'none';
      console.log('Image size exeding');
      imagesizelimit.style.display = 'block';
    }
  }
  _handleReaderLoaded(e) {
    const reader = e.target;
    this.imageSrc = reader.result;
    const fName = this.currentUser.email.split('@');
    const imageData = {
      email: this.currentUser.email,
      filename: fName[0] + fName[1] + '.jpg',
      imageBase64: this.imageSrc
    };
    console.log(imageData);
    this.shared.uploadUserProfileImage(imageData).subscribe(async (event) => {
        const res = await JSON.parse(JSON.stringify(event));
        const imagesizelimit = (document.getElementById('imagesizelimit') as HTMLInputElement);
        // const uploadunsuccessful = (document.getElementById('upload-unsuccessful') as HTMLInputElement);
        if (res.status === 200) {
          this.percentDone = 100;
          this.uploadSuccess = true;
          // uploadunsuccessful.style.display = 'none';
          imagesizelimit.style.display = 'none';
          this.imageLocation = res.data;
          this.currentUser.profileurl = this.imageLocation;
          localStorage.setItem('currentUser', JSON.stringify(this.currentUser));
          this.authenticationService.sharedUpdatedUserDetails(this.currentUser);

          this.fetchDataOncard();
        } else {
          this.uploadSuccess = false;
        }
      });
  }
  fileUpload() {
     console.log('file upload');
    (<HTMLElement>document.getElementsByClassName('file-upload')[0]).click();
  }
  uploadImage() {
    const mailerr = <HTMLElement>document.getElementById('user-image');
    mailerr.style.display = 'block';
    const userimagestyle = <HTMLElement>document.getElementById('userImageStyle');
    userimagestyle.style.opacity = '0.4';
    userimagestyle.style.cursor = 'pointer';
  }
  removeThumbnailImage() {
    const mailerr = <HTMLElement>document.getElementById('user-image');
    mailerr.style.display = 'none';
    const userimagestyle = <HTMLElement>document.getElementById('userImageStyle');
    userimagestyle.style.opacity = '';
  }
}

