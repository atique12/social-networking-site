import { Component, OnInit } from '@angular/core';

import { AuthenticationService } from '../../../auth/authentication.service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { SharedservicesService } from '../../../services/sharedservices.service';
import { Subscription } from 'rxjs';
import { Userdetails } from '../../../user.model';

@Component({
  selector: 'app-list-profiles',
  templateUrl: './list-profiles.component.html',
  styleUrls: ['./list-profiles.component.scss']
})
export class ListProfilesComponent implements OnInit {
  unamedurl: any;
  currentUser: Userdetails;
  currentUserSubscription: Subscription;
  users: Userdetails[] = [];
  listsUser = [];

  constructor(private router: Router, private authenticationService: AuthenticationService, 
    private service: SharedservicesService, private http: HttpClient) {
    this.currentUserSubscription = this.authenticationService.currentUser.subscribe(user => {
      this.currentUser = user;
    });
  }
  ngOnInit() {
    this.service.getAllProfileInformation().subscribe((res) => {
      const result = JSON.parse(JSON.stringify(res));
      result.data.filter((fs) => {
         if (fs.email !== this.currentUser.email) {
           this.listsUser.push(fs);
         }
      });
    });
  }
  onImgError() {
    this.unamedurl = 'https://test-live-projects.s3.ap-south-1.amazonaws.com/notnamed.png';
    this.listsUser.filter((fs) => {
      if (!fs.profileurl) {
        fs.profileurl = this.unamedurl;
      }
    });
  }
}
