import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SharedservicesService } from '../../../services/sharedservices.service';
import { AuthenticationService } from '../../../auth/authentication.service';
import { HttpClient } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { Userdetails } from '../../../user.model';

@Component({
  selector: 'app-profile-settings',
  templateUrl: './profile-settings.component.html',
  styleUrls: ['./profile-settings.component.css']
})
export class ProfileSettingsComponent implements OnInit {
  name: any;
  headline: any;
  phone: any;
  dob: any;
  website: any;
  github: any;
  facebook: any;
  youtube: any;
  twitter: any;
  userbio: any;
  currentUser: Userdetails;
  currentUserSubscription: Subscription;
  users: Userdetails[] = [];
  websiteValid = false;
  facebookValid = false;
  youtubeValid = false;
  twitterValid = false;
  githubValid = false;
  constructor(private router: Router, private authenticationService: AuthenticationService,
    private shared: SharedservicesService, private http: HttpClient) {
    this.currentUserSubscription = this.authenticationService.currentUser.subscribe(user => {
      this.currentUser = user;
    });
  }

  ngOnInit() {
    const dataSet = {
      email: this.currentUser.email
    };
    this.shared.getLoggedUserInformation(dataSet).subscribe((res) => {
       const result = JSON.parse(JSON.stringify(res));
       this.name = result.data.name;
       this.headline = result.data.headline;
       this.dob = result.data.dob;
       this.userbio = result.data.userbio;
       this.phone = result.data.phone;
       this.website = result.data.website;
       this.facebook = result.data.facebook;
       this.youtube = result.data.youtube;
       this.twitter = result.data.twitter;
       this.github = result.data.github;
       this.dob = new Date(this.dob).toISOString().split('T')[0];
       this.validateUrl(this.website, 'website');
       this.validateUrl(this.youtube, 'youtube');
       this.validateUrl(this.facebook, 'facebook');
       this.validateUrl(this.github, 'github');
       this.validateUrl(this.twitter, 'twitter');
    });
  }
  validateUrl(data, profile) {
    const pattern = /^https?:\/\//;
    if (!pattern.test(data)) {
      const mailerr = <HTMLElement>document.getElementById('errmsg-validate-url');
      mailerr.style.display = 'block';
    } else {
      if (profile === 'github') {
        this.githubValid = true;
      } if (profile === 'twitter') {
        this.twitterValid = true;
      } if (profile === 'facebook') {
        this.facebookValid = true;
      } if (profile === 'website') {
        this.websiteValid = true;
      } if (profile === 'youtube') {
        this.youtubeValid = true;
      }
    }
  }
  updateProfile() { //atique
    const errmsg = <HTMLElement>document.getElementById('errmsg');
    const successmsg = <HTMLElement>document.getElementById('successmsg');
    const mailerr = <HTMLElement>document.getElementById('errmsg-validate-url');
    const dataSet = {
      email: this.currentUser.email,
      name: this.name,
      headline: this.headline,
      phone: this.phone,
      dob: this.dob,
      userbio: this.userbio,
      website: this.website,
      github: this.github,
      facebook: this.facebook,
      youtube: this.youtube,
      twitter: this.twitter
    };
    if (this.name && this.headline && this.dob && this.phone && this.userbio &&
      this.websiteValid && this.githubValid && this.twitterValid && this.youtubeValid && this.facebookValid) {
      mailerr.style.display = 'none';
      errmsg.style.display = 'none';
      this.shared.userProfileInformationUpdate(dataSet).subscribe((data) => {
        const res = JSON.parse(JSON.stringify(data));
        if (res.status === 200) {
          successmsg.style.display = 'block';
          // change the data in the same state
          console.log(this.currentUser);
          this.currentUser.headline = this.headline;
          this.currentUser.name = this.name;
          this.currentUser.userbio = this.userbio;
          // this.currentUser.email
          localStorage.setItem('currentUser', JSON.stringify(this.currentUser));
          this.authenticationService.sharedUpdatedUserDetails(this.currentUser);

          this.router.navigate(['/app/dashboard']);
        } else {
          mailerr.style.display = 'block';
        }
      });
    } else {
      mailerr.style.display = 'block';
      errmsg.style.display = 'block';
      successmsg.style.display = 'none';
    }
  }
}
