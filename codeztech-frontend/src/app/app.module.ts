import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './modules/dashboard/dashboard.component';
import { DisqusModule } from 'ngx-disqus';
import { ErrorInterceptor } from './_helpers/error.interceptor';
import { FooterComponent } from './layout/footer/footer.component';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { HeaderComponent } from './layout/header/header.component';
import { HttpClientModule } from '@angular/common/http';
import { JwtInterceptor } from './_helpers/jwt.interceptor';
import { LayoutComponent } from './layout/layout.component';
import { ListProfilesComponent } from './modules/dashboard/list-profiles/list-profiles.component';
import { LoginComponent } from './modules/login/login.component';
import { MainComponent } from './layout/main/main.component';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material';
import {MatTableModule} from '@angular/material/table';
import { MatTooltipModule } from '@angular/material/tooltip';
//Modules
import { MaterialModule } from './material.module';
import { NgModule } from '@angular/core';
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { ProfileComponent } from './modules/dashboard/profile/profile.component';
import { ProfileSettingsComponent } from './modules/dashboard/profile-settings/profile-settings.component';
//Routing
import { ROUTING } from './app.routing';
import { RegisterComponent } from './modules/register/register.component';
import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';
//Services
import { SharedservicesService } from './services/sharedservices.service';
import {SuiModule} from 'ng2-semantic-ui';
import { UserWallsComponent } from './modules/dashboard/user-walls/user-walls.component';
import { YoutubePlayerModule } from 'ngx-youtube-player';

//Component
@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    ProfileSettingsComponent,
    ListProfilesComponent,
    ProfileComponent,
    UserWallsComponent,
    LoginComponent,
    RegisterComponent,
    LayoutComponent,
    HeaderComponent,
    MainComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    MaterialModule,
    MatTooltipModule,
    MatTableModule,
    MatDatepickerModule,
    MatNativeDateModule,
    ROUTING,
    DisqusModule,
    DisqusModule.forRoot('web-123'),
    YoutubePlayerModule,
    BrowserAnimationsModule,
    CommonModule,
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    ScrollToModule.forRoot(),
    NgbModalModule,
    // KeycloakAngularModule,
    // StaticModule,
    MaterialModule,
    SuiModule
  ],
  providers: [
    SharedservicesService,
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
