const mongoose = require('mongoose');
require('../models/userschema');
require('../models/articleschema');
require('../models/usercommentschema');
const Userdetail = mongoose.model('userdetail');
const UserArticleSubmit = mongoose.model('articleInformation');
const UserCommentInformation = mongoose.model('userCommentInformation');

const chalk = require('chalk');
const jwt = require('jsonwebtoken');
const config = require('config');
var bucketName = config.AWS.bucket;
var AWS = require('aws-sdk');
var s3Bucket = new AWS.S3();
var _this = this;

AWS.config.update({
  accessKeyId: config.AWS.accessKeyId,
  secretAccessKey: config.AWS.secretAccessKey,
  region: config.AWS.region
});

// Product Image upload
exports.imageCreate = (req, res, next) => {
  var buf = Buffer.from(req.body.imageBase64.replace(/^data:image\/\w+;base64,/, ""),'base64');
  var data = {
    Key: req.body.filename, // type is not required
    Body: buf,
    Bucket: bucketName,
    ContentEncoding: 'base64',
    ContentType: 'image/jpeg'
  };

  s3Bucket.upload(data, function(err, data){
      if (err) {
        console.log(err);
        console.log(chalk.red('Error uploading data: ', data));
        res.json({
          status: 400,
          message: 'Error uploading data!'
        })
      } else {
        console.log(chalk.green('succesfully uploaded the image!'));
        var isUpdatedUrl = _this.updateUserProfileInDataBase(req.body.email, data.Location);
        if(isUpdatedUrl) {
          res.json({
            status: 200,
            data: data.Location,
            message: 'done'
          })
        } else {
           res.send({
             status:400,
             message: 'S3 Error!'
           })
        }
      }
  });
};

exports.updateUserProfileInDataBase = (email, imagePath) => {
   console.log(email, imagePath);
   Userdetail.findOne({'email': email}, async( err, userInfo) => {
    if (userInfo!==null) {
      userInfo.profileurl = imagePath;
      userInfo.updatedDate = new Date();
      var userInfoSave = new Userdetail(userInfo);
      userInfoSave.save().then(dataInfo => {
           console.log('Profile pic updated!');
        }).catch(err => {
          console.log('Err, User not found, Profile pic updated falied!');
      })
       // User Article tables updated
       UserArticleSubmit.updateMany({'email': email}, {"$set":{"profileurl": userInfo.profileurl}}, {"multi": true}, (err, writeResult) => {
        if(!err) {
          console.log(chalk.green('User Profileurl updated in article details table !'));
        } else {
          console.log(chalk.red('Err, User Profileurl cant updated in article details table !'));
        }
      });

      // User Comments table update -
      UserCommentInformation.updateMany({'email': email}, {"$set":{"profileurl": userInfo.profileurl}}, {"multi": true}, (err, writeResult) => {
        if(!err) {
          console.log(chalk.green('User Profileurl updated in comments details table !'));
        } else {
          console.log(chalk.red('Err, User Profileurl cant updated in comments details table !'));
        }
      });
    }
  })
  return true;
}

exports.articlePost = (req, res, next) => {
  console.log(chalk.bgYellowBright("---------------- Published Article ----------------"));
  console.log(req.body);
  var userArticleSave = new UserArticleSubmit(req.body);
        userArticleSave.save().then(dataInfo => {
          res.send({
            message: 'User Article Submitted !!',
            status: 200,
            data: dataInfo
          })
        }).catch(err => {
            res.send({
              message: 'Article create Failed !!',
              status: 400,
              err: err
            });
      })
};

exports.getAllArticlePost = (req, res, next) => {
  console.log(chalk.bgYellowBright("---------------- Get Article ----------------"));
  console.log(req.body);
  UserArticleSubmit.find((err, listArticle)=> {
    if (err)
      console.log(err)
    else {
      const result = JSON.parse(JSON.stringify(listArticle));
      for(var i = 0; i < listArticle.length; i++) {
        if(req.body.email === result[i].email) {
          listArticle[i].isOwnsPost = true; 
        } else {
          listArticle[i].isOwnsPost = false; 
        }
      }
      
      // Reverse Array in order to get the Article at first - 
      var reserveData = [];
      for(var i = listArticle.length-1; i >= 0; i--) {
        reserveData.push(listArticle[i]);
      }
      res.status(200).send({
        message: 'Succesfully fetch Record!',
        data: reserveData
      });
    }
  });
};

exports.userCommentsOnArticles = (req, res, next) => {
  console.log(chalk.bgYellowBright("---------------- User Comments On Article ----------------"));
  console.log(req.body);
  UserArticleSubmit.findOne({'_id': req.body.articleId}, async( err, userCommentsInfo) => {
    if (userCommentsInfo!==null) {
      //userCommentsInfo.comments = ;
      
      //User Comments - 
      var userComments = {
        articleId: req.body.articleId,
        article: req.body.article,
        articleAuthorEmail: req.body.articleAuthorEmail, 
        email: req.body.commentedUserEmail,
        name: req.body.commentedUserName,
        headline: req.body.commentedUserHeadline,
        profileurl: req.body.commentedUserProfileUrl,
        comments: req.body.commentedUserComments,
        createdAt: req.body.commentedUserCreatedAt,
        updatedDate: req.body.commentedUserUpdatedAt,
      }
      userCommentsInfo.updatedDate = new Date();
      var userInfoSave = new UserCommentInformation(userComments);
      userInfoSave.save().then(dataInfo => {
          res.send({
            message: 'User Comments Updated !!',
            status: 200,
            data: dataInfo
          })
        }).catch(err => {
          res.send({
            message: 'Update Failed !!',
            status: 400,
            err: err
          });
      })
    } else {
      res.status(400).send({
        message: 'Err, Article is missing !!',
        status: 400,
      })
    }
  });
};

exports.userFetchAllCommentsOnArticles = (req, res, next) => {
  console.log(chalk.bgYellowBright("---------------- User Fetch On Comments Based on Article ----------------"));
  UserCommentInformation.find(( err, listCommentsOnArticle) => {
    const result = JSON.parse(JSON.stringify(listCommentsOnArticle));
      for(var i = 0; i < listCommentsOnArticle.length; i++) {
        if(req.body.email === result[i].email) {
          listCommentsOnArticle[i].isOwnsPost = true; 
        } else {
          listCommentsOnArticle[i].isOwnsPost = false; 
        }
      }
    res.status(200).send({
       message: 'user comments!',
       data: listCommentsOnArticle
    })
  }) 
};

exports.userDeleteMainArticles = (req, res, next) => {
  console.log(chalk.bgYellowBright("---------------- User Delete Article ----------------"));
  console.log(req.body);
  UserArticleSubmit.findByIdAndRemove({_id: req.body.id}, (err, product) => {
    if (err)
      console.log('Err, deleting Main Articles');
    else
      console.log('Main Article Remove sucessfully');
  });

  UserCommentInformation.deleteMany({articleId: req.body.id}, (err, product) => {
    if (err)
      console.log('Err, deleting Sub, Articles');
    else
      console.log('Main Articles, Related Comments Remove sucessfully');
  });

  res.status(200).send({
     status: 200,
     message: 'Article deleted Successfully'
  })
}

exports.userUpdateMainArticles = (req, res, next) => {
  console.log(chalk.bgYellowBright("---------------- User Update Article ----------------"));
  console.log(req.body);
  UserArticleSubmit.updateOne({_id: req.body.id}, {"$set":{"articledesc": req.body.article, "updatedDate": new Date()}}, {"multi": true}, (err, product) => {
    if (err)
      console.log('Err, deleting Main Articles');
    else
      console.log('Main Article Update sucessfully');
  });

  UserCommentInformation.updateMany({articleId: req.body.id}, {"$set":{"article": req.body.article}}, {"multi": true}, (err, product) => {
    if (err) {
      console.log('Err, updating Sub, Articles');
    }
    else {
      console.log('Main Articles, Related Comments Updated sucessfully');
    }
  });

  res.status(200).send({
     status: 200,
     message: 'Article udpated Successfully'
  })
};

exports.userDeleteSubComments = (req, res, next) => {
  console.log(chalk.bgYellowBright("---------------- User Delete Sub - Article ----------------"));
  console.log(req.body);
  UserCommentInformation.findByIdAndRemove({_id: req.body.id}, (err, product) => {
    if (err) {
      res.status(400).send({
        status: 400,
        message: 'Err, deleting Main Articles'
      })
    }
    else {
      res.status(200).send({
        status: 200,
        message: 'Main Article Remove sucessfully'
      })
    }
  });
  
};

exports.userRegister = (req, res, next) => {
  console.log(chalk.bgYellowBright("---------------- Submit Information ----------------"));
  console.log(req.body);
  
  Userdetail.findOne({'email': req.body.email}, async( err, userInfo) => {
    if (userInfo!==null) {
      res.status(400).send(
        {
            'status': 400,
            'message': 'User Exists in our database!',
        });
    } else {
      var userdetails = new Userdetail(req.body);
      userdetails.save()
        .then(user => {
          console.log(chalk.bgYellowBright('User Registered!'))   
          res.status(200).send(
            {
                'status': 200,
                'message': 'Added Successfully',
                'data': user
            });
        })
        .catch(err => {
          res.status(400).send('Failed to create new record');
      });
    }
  });
};


exports.userLogin = (req, res, next) => {
  console.log(chalk.bgYellowBright("---------------- User Login ----------------"));
  console.log(req.body);

  var query = { email : req.body.email, password: req.body.password };
  console.log(query);

  Userdetail.find(query, (err, result) => {
    console.log(result);
    console.log(result.length);
    if (result.length === "0") {
      console.log('Invalid User');
      //res.json('Invalid user name and password');
      return res.status(401).send('Invalid User and/or Password.');
    }
    else  {
        var userResult = JSON.stringify(result);
        userResult = userResult.replace(/(^\[)/, '');
        userResult =  userResult.replace(/(\]$)/, '');
        try {
           var userdata = JSON.parse(userResult);
        } catch(e) {
            return res.status(401).send('Invalid User and/or Password.');
        }
        var tokenExpiresTime = Math.floor(Date.now() / config.tokenExpires) + (60 * 60); //valid for 2 hr
        jwt.sign({email: req.body.email}, config.secret, {expiresIn: tokenExpiresTime}, (err, utoken) => {
            return res.json({
                email: req.body.email,
                name : userdata['name'],
                userbio : userdata['userbio'],
                headline : userdata['headline'],
                profileurl : userdata['profileurl'],
                token: utoken
            });
        });
      }
  });
};

exports.userLoginProfileDetails = (req, res, next) => {
  console.log(chalk.bgYellowBright("---------------- Get Login User Profile ----------------"));
  console.log(req.body);
  Userdetail.findOne({'email': req.body.email}, async( err, userInfo) => {
    res.status(200).send({
       message: 'user details!',
       data: userInfo
    })
  })
}

exports.userProfileUpdate = (req, res, next) => {
  console.log(chalk.bgYellowBright("---------------- User Profile Update ----------------"));
  console.log(req.body);
  Userdetail.findOne({'email': req.body.email}, async( err, userInfo) => {
    if (userInfo!==null) {
      userInfo.name = req.body.name;
      userInfo.headline = req.body.headline;
      userInfo.phone = req.body.phone;
      userInfo.dob = req.body.dob;
      userInfo.userbio = req.body.userbio;
      userInfo.website = req.body.website;
      userInfo.github = req.body.github;
      userInfo.facebook = req.body.facebook;
      userInfo.youtube = req.body.youtube;
      userInfo.twitter = req.body.twitter;
      userInfo.profileurl = userInfo.profileurl;

      userInfo.updatedDate = new Date();
      var userInfoSave = new Userdetail(userInfo);
      //Logic here
      userInfoSave.save().then(dataInfo => {
          console.log(chalk.green('User Information updated in user details table !'));
        }).catch(err => {
          console.log(chalk.red('Err, User Information cant updated in user details table !'));
      })

      // User Article tables updated
      UserArticleSubmit.updateMany({'email': req.body.email}, {"$set":{"name": req.body.name, "headline": req.body.headline}}, {"multi": true}, (err, writeResult) => {
        if(!err) {
          console.log(chalk.green('User Information updated in article details table !'));
        } else {
          console.log(chalk.red('Err, User Information cant updated in article details table !'));
        }
      });

      // User Comments table update -
      UserCommentInformation.updateMany({'email': req.body.email}, {"$set":{"name": req.body.name, "headline": req.body.headline}}, {"multi": true}, (err, writeResult) => {
        if(!err) {
          console.log(chalk.green('User Information updated in comments details table !'));
        } else {
          console.log(chalk.red('Err, User Information cant updated in comments details table !'));
        }
      });

      res.send({
        message: 'User Information Updated !!',
        status: 200,
      })
    } else {
      res.status(400).send({
        message: 'User Email Information missing !!',
        status: 400,
      })
    }
  });
}

exports.userTrackReactions = (req, res, next) => { //atique
  console.log(chalk.bgYellowBright("---------------- User Track Reactions ----------------"));
  console.log(req.body);
  // UserArticleSubmit.updateOne({_id: req.body.id}, {"$set":{"articledesc": req.body.article, "updatedDate": new Date()}}, {"multi": true}, (err, product) => {
  //   if (err)
  //     console.log('Err, deleting Main Articles');
  //   else
  //     console.log('Main Article Update sucessfully');
  // });
  UserArticleSubmit.findOne({'_id':  req.body.id}, async( err, userCommentsInfo) => {
    if (userCommentsInfo!==null) {
       var countLike = 0, disLike = 0;
       console.log(userCommentsInfo.userLikeDetails.length);
       console.log(userCommentsInfo.userLikeDetails[0]);
       if(userCommentsInfo.userLikeDetails.length === 1) {
        if(userCommentsInfo.userLikeDetails[0] === '') {
          userCommentsInfo.userLikeDetails = [];
        }
       }
       if(userCommentsInfo.userLikeDetails.length === 0) {
         userCommentsInfo.userLikeDetails.push({
            'userEmail': req.body.loggedUserEmail,
            'status': req.body.isTrue,
         });
         if(req.body.isTrue) {
          userCommentsInfo.totalLike = 1;
         } else {
          userCommentsInfo.totalDisLike = 1;
         }
       } else {
         var isOlduser = false;
         for(var i=0;i<userCommentsInfo.userLikeDetails.length;i++) {
           if(userCommentsInfo.userLikeDetails[i].userEmail === req.body.loggedUserEmail) {
            userCommentsInfo.userLikeDetails[i].status = req.body.isTrue;
             isOlduser = true;
           }
         }
         if(!isOlduser) {
            userCommentsInfo.userLikeDetails.push({
              'userEmail': req.body.loggedUserEmail,
              'status': req.body.isTrue,
            })
          }
          // After calculation User Likes and Dislikes Count - 
          
          for(var i=0;i<userCommentsInfo.userLikeDetails.length;i++) {
             if(userCommentsInfo.userLikeDetails[i].status) {
               countLike++;
             } else if(!userCommentsInfo.userLikeDetails[i].status){
               disLike++;
             }
          }
          userCommentsInfo.totalLike = countLike;
          userCommentsInfo.totalDisLike = disLike;
        }
        console.log(userCommentsInfo);
        var userInfoSave = new UserArticleSubmit(userCommentsInfo);
        userInfoSave.save().then(dataInfo => {
            res.send({
              message: 'User Comments Updated !!',
              status: 200,
              data: dataInfo
            })
          }).catch(err => {
            res.send({
              message: 'Update Failed !!',
              status: 400,
              err: err
            });
        })
    }
  });
}

exports.getAllUserData = (req, res, next) => {
    console.log(chalk.bgYellowBright("---------------- Get Information ----------------"));
    console.log(req.body);
    Userdetail.find((err,product)=> {
    if (err)
      console.log(err)
    else
      res.status(200).send({
          message: 'Succesfully fetch Record!',
          data: product
      });
    });
}