const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const app = express();

app.use(bodyParser.json());

app.use(cors());

// Authentication of user
const authentication = require('../authentication/auth');

// Middleware
// const middleware = require('../middleware/validatingApi');

// Controller
const controllers = require('../controllers/userInformation');

// Sample API testing
app.get('/', (req, res) => {
  res.send({
     status:200,
     message:'App is working fine!'
  });
});

app.post('/users/register', (req, res, next) => {
  controllers.userRegister(req, res);
});

app.post('/users/login', (req, res, next) => {
  controllers.userLogin(req, res);
});

app.post('/users/profile/update', [authentication.authUser], (req, res, next) => {
  controllers.userProfileUpdate(req, res);
});

app.post('/users/profile/picture/upload', [authentication.authUser], (req, res, next) => {
  controllers.imageCreate(req, res);
});

app.post('/users/article/post', [authentication.authUser], (req, res, next) => {
  controllers.articlePost(req, res);
});

app.post('/users/article/get', [authentication.authUser], (req, res, next) => {
  controllers.getAllArticlePost(req, res);
});

app.post('/users/article/comments', [authentication.authUser], (req, res, next) => {
  controllers.userCommentsOnArticles(req, res);
});

app.post('/users/article/fetchall/comments', [authentication.authUser], (req, res, next) => {
  controllers.userFetchAllCommentsOnArticles(req, res);
});

app.post('/users/delete/articles', [authentication.authUser], (req, res, next) => {
  controllers.userDeleteMainArticles(req, res);
});

app.post('/users/udpate/articles', [authentication.authUser], (req, res, next) => {
  controllers.userUpdateMainArticles(req, res);
});

app.post('/users/delete/comments', [authentication.authUser], (req, res, next) => {
  controllers.userDeleteSubComments(req, res);
});

app.post('/users/track/reactions', [authentication.authUser], (req, res, next) => {
  controllers.userTrackReactions(req, res);
});

app.post('/logged/users', authentication.authUser, (req, res, next) => {
  controllers.userLoginProfileDetails(req, res);
});

app.get('/users', authentication.authUser, (req, res, next) => {
  controllers.getAllUserData(req, res);
});

module.exports = app;