const mongoose = require('mongoose');

var Schema = mongoose.Schema;

var articleInformation = new mongoose.Schema({
  email: {type: String, default: ''},
  name: {type: String, default: ''},
  headline: {type: String, default: ''},
  profileurl: {type: String, default: ''},
  articledesc: {type: String, default: ''},
  totalLike: {type: Number, default: 0},
  totalDisLike: {type: Number, default: 0},
  userLikeDetails: {type: Array, default: ''},
  comments: {type: String, default: ''},
  isOwnsPost: {type: Boolean, default: false},
  createdAt: { type: Date, default: Date.now},
  updatedDate: {type: Date, default: Date.now},
});

var articleInformation = mongoose.model('articleInformation', articleInformation);
module.exports = articleInformation;
