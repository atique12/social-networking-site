const mongoose = require('mongoose');

var Schema = mongoose.Schema;

var userdetail = new mongoose.Schema({
  name: {type: String},
  phone: {type: String},
  profileurl: {type: String, default: ''},
  headline: {type: String, default: ''},
  dob: {type: Date, default: ''},
  email: {type: String, default: ''},
  password: {type: String, default: ''},
  userbio: {type: String, default: ''},
  website: {type: String, default: ''},
  github:{type: String, default: ''},
  facebook:{type: String, default: ''},
  youtube: {type: String, default: ''},
  twitter: {type: String, default: ''},
  createdAt: { type: Date, default: Date.now},
  updatedDate: {type: Date, default: Date.now},
});

var userdetail = mongoose.model('userdetail', userdetail);
module.exports = userdetail;
