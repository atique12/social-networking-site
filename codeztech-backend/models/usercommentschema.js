const mongoose = require('mongoose');

var Schema = mongoose.Schema;

var userCommentInformation = new mongoose.Schema({
  articleId: {type: String, default: ''},
  article: {type: String, default: ''},
  articleAuthorEmail: {type: String, default: ''},
  email: {type: String, default: ''},
  name: {type: String, default: ''},
  headline: {type: String, default: ''},
  profileurl: {type: String, default: ''},
  isOwnsPost: {type: Boolean, default: false},
  comments: {type: String, default: ''},
  createdAt: { type: Date, default: Date.now},
  updatedDate: {type: Date, default: Date.now},
});

var userCommentInformation = mongoose.model('userCommentInformation', userCommentInformation);
module.exports = userCommentInformation;
